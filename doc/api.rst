API
===
.. autoclass:: apgpio.GPIO
    :members:
    :special-members:

.. vim: sw=4:et:ai
