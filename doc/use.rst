Using the Library
=================
The module provides :py:class:`apgpio.GPIO` class to read status of a GPIO
pin. The class implements :py:meth:`apgpio.GPIO.read_async` coroutine to
retrieve status of a pin and the coroutine returns only when status of a
pin changed.

Example usage::

    async def read_data(pin):
        while True:
            status = await pin.read_async()
            print(status)

    loop = asyncio.get_event_loop()
    pin = btzen.GPIO(23)
    loop.run_until_complete(read_data(pin))

.. vim: sw=4:et:ai
